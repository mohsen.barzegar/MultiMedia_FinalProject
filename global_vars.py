# Golobal variables are here

'''Global variables used by functions in different files.'''

component= {} # Horizontal downsampling factor,
              # Vertical downsampling factor,
              # Quantization Table,
              # AC and DC huffman table,
              # used for each component in image

			  
# quantization tables
q_table= [[], [], [], []]

# pointer to generator a function that provides bits
bit_stream = 0;


# DC and AC huffman tables
h_ac_tables= [{}, {}, {}, {}]
h_dc_tables= [{}, {}, {}, {}]


XYP= 0, 0, 0 # Width, Height, Precision



# number of components, probably 3
num_components= 0

# number of read MCUs, just for debugging
mcus_read= 0

# DC value for previous block, for each component
dc= []

#
inline_dc= 1

# number of bits used for IDCT
idct_precision= 8

# End Of Image flag
EOI= False

# data holder, many kinds of data!
data= []

# pointer to generator a function that provides bits
bit_stream = 0;