function [ output_args ] = read_sof( fid )


global components;
global XYP;

% Frame header length
Lf = fread(fid, 1, '*uint16' );
%Lf = swapbytes(Lf);
Lf = Lf - 2;

% Sample Precision
P = fread(fid, 1, '*uint8' );
Lf = Lf - 1;

% Number of Lines
Y = fread(fid, 1, '*uint16' );
%Y = swapbytes(Y);
Lf = Lf - 2;

% Number of samples per line
X = fread(fid, 1, '*uint16' );
%X = swapbytes(X);
Lf = Lf - 2;

XYP = [X Y P];

% Number of image components in frame
Nf = fread(fid, 1, '*uint8' );
Lf = Lf - 1;


while Lf > 0
    
    % Component identifier
    C = fread(fid, 1, '*uint8' );
    % Both sampling factor together
    V = fread(fid, 1, '*uint8' );
    % Quantization table destination selector
    Tq = fread(fid, 1, '*uint8' );
    
    Lf = Lf - 3;
    
    H = bitsrl(V, 4);
    V = bitand(V, hex2dec('0F'));
    
    components(C, 1:3) = [H V Tq];
    
    
    
end




end

