function [ byte ] = read_byte( fid )


global dc;
global EOI;
global inline_dc;
global num_components;


% read a byte from stream
byte = fread(fid, 1, '*uint8' );

if byte == hex2dec('FF')
    afterFF = fread(fid, 1, '*uint8' );
    
    if afterFF == hex2dec('00') %byte stuffing
        byte = uint8(hex2dec('FF'));
        
    elseif afterFF == hex2dec('D9') % EOI
        EOI = 1;
        
    elseif (hex2dec('D0') <= afterFF) && ( afterFF <= hex2dec('D7') )
        dc = zeros(1, num_components);
        byte = fread(fid, 1, '*uint8' );
        
    else
        byte = fread(fid, 1, '*uint8' );
        
    end
    
end


end

