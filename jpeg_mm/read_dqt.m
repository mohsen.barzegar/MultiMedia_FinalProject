function [ output_args ] = read_dqt( fid )


global q_table;

Lq = fread(fid, 1, '*uint16' );
%Lq = swapbytes(Lq);
Lq = Lq - 2;

 % there are multiple quantization tables in a DQT segment
while Lq > 0
    
    % read Tq and Pq
    Tq = fread(fid, 1, '*uint8' );
    Pq = bitsrl(Tq, 4);
    Tq = bitand(Tq, hex2dec('0F'));
    Lq = Lq - 1;
    
    
    % Specifies the precision of the Q_k values
    % 0 indicates 8-bit Q_k
    % 1 indicates 16-bit Q_k
    if Pq == 0
        for m = 1:64
            q_table(m,Tq+1) = fread(fid, 1, '*uint8' );
            Lq = Lq - 1;
        end

    else
        for m = 1:64
            temp = fread(fid, 1, '*uint16' );
            q_table(m, Tq+1) = temp;%swapbytes(temp);
            Lq = Lq - 2;
        end

    end
end



end

