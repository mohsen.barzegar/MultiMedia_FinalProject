function [ huffcode ] = huffman_codes( huffsize )

% Calculate 
    
idx = 1;
code = uint32(0);
for m = 1:16
    si = huffsize(m);
    for k = 1:si
        huffcode(idx,:) = [m , code];
        idx = idx + 1;
        code = code + 1;
        
    end
    
    code = bitsll(code, 1);
end


end

