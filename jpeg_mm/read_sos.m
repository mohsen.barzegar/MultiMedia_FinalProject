function [ output_args ] = read_sos( fid )

global components;
global num_components;
global dc;

% Scan header length
Ls = fread(fid, 1, '*uint16' );
%Ls = swapbytes(Ls);
Ls = Ls - 2;

% Number of image components in scan
Ns = fread(fid, 1, '*uint8' );
Ls = Ls - 1;

num_components = Ns;    

for i = 1:Ns
    Cs = fread(fid, 1, '*uint8' );
    Ta = fread(fid, 1, '*uint8' );
    Ls = Ls - 2;
    
    Td = bitsrl(Ta, 4);
    Ta = bitand(Ta, hex2dec('0F'));
    
    components(Cs, 4:5) = [Td Ta];
    
end

Ss = fread(fid, 1, '*uint8' );
Se = fread(fid, 1, '*uint8' );
A  = fread(fid, 1, '*uint8' );
Ls = Ls - 3;

if Ss ~= 0 || Se ~= 63 || A ~= 0
    disp('There is a problem in read_sos');
end

dc = zeros(1, num_components);
 
end

