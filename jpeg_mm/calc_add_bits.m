function [ output ] = calc_add_bits( len, val )


% the first bit following tha magnitude
% specifies the sign, 0 means negative
% 1 means positive
if bitand(val, bitsll(1, len-1)) ~= 0
    % do nothing!
else
    val = int32(val) - (bitsll(1, len) - 1);
end

output = int32(val);


end

