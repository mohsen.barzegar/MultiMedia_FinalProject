function [ output ] = get_bits( no_bits, fid )

% remaining bits in current bytes
global rem_bits;
% current byte
global byte;

if rem_bits == 0
    byte = read_byte(fid);
    rem_bits = 8;
end

no_bits = int32(no_bits);

output = uint32(0);    
    
while no_bits > rem_bits
    
    output = bitsll(output, rem_bits); % make space for new bits
    output = bitor(output, uint32(bitsrl(byte, 8 - rem_bits))); % insert new bits into output
    no_bits = no_bits - rem_bits; 
    byte = read_byte(fid);
    rem_bits = 8;
    
end

% now we have: no_bits <= rem_bits
output = bitsll(output, no_bits); % make space for new bits
output = bitor(output, uint32(bitsrl(byte, 8 - no_bits)));
byte = bitsll(byte, no_bits);
rem_bits = rem_bits - no_bits;


% there is no need to read new byte


end

