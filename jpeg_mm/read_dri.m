function [ Ri ] = read_dri( fid )

% Define restart interval segment length -> it's always 4
Lr = fread(fid, 1, '*uint16' );
%Lr = swapbytes(Lr);

if Lr ~= 4
    disp('Lr in DRI is not 4!');
end

%Restart interval
Ri = fread(fid, 1, '*uint16' );
Ri = swapbytes(Ri);



end

