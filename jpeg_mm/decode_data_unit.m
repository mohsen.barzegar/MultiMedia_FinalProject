function [ data ] = decode_data_unit( fid, comp_num )


global dc;
global components;
global huffman_dc_tables;
global huffman_ac_tables;
global inline_dc;
global du;

du = du + 1;

comp = components(comp_num, :);

huff_table = huffman_dc_tables{comp(4)+1}; %% 4 -> Td
huff_table = cell2mat(huff_table);

coeff_cnt = 1;
data = zeros(64,1);

while coeff_cnt <= 64

    key = uint32(0);
    for bits = 1:16
        
        key = bitsll(key, 1);
        val = get_bits(1, fid);
        key = bitor(key, val);
        
        [foundr foundc] = find(huff_table(:,1) == bits &  huff_table(:,2) == key,1);
        if foundr
            key_len = huff_table(foundr, 3);
            break;
        elseif bits == 16
            disp('Huffman code not found!');
        end
        
    end
    
    huff_table = huffman_ac_tables{comp(5)+1}; % Ta
    huff_table = cell2mat(huff_table);
    
    
    % 16 zero coefficients
    if key_len == hex2dec('F0')
         data(coeff_cnt:coeff_cnt+15) = 0;
         coeff_cnt = coeff_cnt + 16;
         continue;
    end
    
    %not dc coefficient
    if coeff_cnt ~= 1
        
        % EOB
        if key_len == 0 
            data(coeff_cnt:64) = 0;
            coeff_cnt = 65;
            break;
        end
        
        
        leading_zeros = bitsrl(key_len, 4);
        if leading_zeros ~= 0
            data(coeff_cnt:coeff_cnt+leading_zeros-1) = 0;
            coeff_cnt = coeff_cnt + leading_zeros;
        end
        
        key_len = bitand(key_len, hex2dec('0F'));
        
    end
    
    
    if coeff_cnt > 64 
        break
    end
    
    
    if key_len ~= 0
        
        % additional bits
        val = get_bits(key_len, fid);
        
        num = calc_add_bits(key_len, val);
        
        if coeff_cnt == 1 && inline_dc ~= 0
            num = num + dc(comp_num);
            dc(comp_num) = num;
        end
        
        data(coeff_cnt) = num;
        
    else
        data(coeff_cnt) = 0;
    end
    
    coeff_cnt = coeff_cnt + 1;
             
end

end

