function [ output_args ] = read_dht( fid )

global huffman_dc_tables;
global huffman_ac_tables;

% Huffman table definition length
Lh = fread(fid, 1, '*uint16' );
%Lh = swapbytes(Lh);
Lh = Lh - 2;


huffsize = zeros(16,1);
while Lh > 0
    
    Th = fread(fid, 1, '*uint8' );
    % Table class � 0 = DC table or lossless table, 1 = AC table.
    Tc = bitsrl(Th, 4);
    % Huffman table destination identifier
    Th = bitand(Th, hex2dec('0F'));
    
    Lh = Lh - 1;
    
    % L_i: Number of Huffman codes of length i
    for Li_cnt = 1:16
        huffsize(Li_cnt) = fread(fid, 1, '*uint8' );
    end
    Lh = Lh - 16;
    
    huffcode = huffman_codes(huffsize);
    
    huffval = zeros(size(huffcode,1),1);
    for m = 1:size(huffcode,1)
        huffval(m) = fread(fid, 1, '*uint8' );
    end
    
    Lh = Lh - size(huffcode,1);
    
    
    if Tc == 0
        A = map_codes_to_values(huffcode, huffval);
        huffman_dc_tables{Th+1} = {A};
        
    else
        A = map_codes_to_values(huffcode, huffval);
        huffman_ac_tables{Th+1} = {A};
    end
    
    
end


end

