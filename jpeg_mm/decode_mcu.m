function [ mcu ] = decode_mcu( fid )


global components;
global num_components;
global EOI;
global mcus_read;


mcu = zeros(1,1,64);

for i = 1:num_components
    comp = components(i,:);
    
    for j = 1:(comp(1)*comp(2)) % H * V
        if ~EOI
            mcu(i,j,:) = decode_data_unit(fid, i);
        end
    end
    
   mcus_read = mcus_read + 1;
   
end

            
        






end

