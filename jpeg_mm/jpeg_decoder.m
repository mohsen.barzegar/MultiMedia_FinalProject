clear

fid=fopen('5.jpg','r', 's'); % opens the file for reading
ch = fread(fid, 1, 'uint8');

global du;
du = 0;

global q_table;
q_table = zeros(64,4);

global components; 
components = zeros(3,5); %H V Tq Td Ta

global num_components;
num_components = 0;

global XYP;
XYP = [0 0 0];

global huffman_dc_tables;
global huffman_ac_tables;

% huffman tables are cells
huffman_dc_tables = {};
huffman_ac_tables = {};

global EOI;
EOI = 0;

global inline_dc;
inline_dc = 0;

global rem_bits;
rem_bits = 0;
global byte;
global dc;

global mcus_read;
mcus_read = 0;


while ch ~= 0
    
    if ch == hex2dec('FF') %new marker
        disp('New Marker');
        ch = fread(fid, 1, 'uint8');
        
        if ch == hex2dec('d8') %SOI
            disp('SOI'); % start of image
        
        elseif ch >= hex2dec('E0') && ch <= hex2dec('EF') % APP
            disp('APP');
            read_app(fid);
            
        elseif ch == hex2dec('DB') % DQT
            disp('DQT'); % define quantization table
            read_dqt(fid);
            
        % notice: DHT marker must be cheked before SOF markers
        elseif ch == hex2dec('C4') % DHT 
            disp('DHT');
            read_dht(fid);
            
        
        % notice: SOF markers must be checked after DHT marker, because
        % 0xFFC4 isn't a SOF marker but it's in range of 0xFFC0 to 0xFFCF
        elseif ch >= hex2dec('C0') && ch <= hex2dec('CF') % SOF 
            disp('SOF');  % start of frame
            ch
            read_sof(fid);
            
        elseif ch == hex2dec('DA') % SOS
            disp('SOS') % start of scan
            read_sos(fid);
            img = decode_scan(fid);
            break;
            
        elseif ch == hex2dec('DD') %DRI
            disp('DRI'); % define restart interval
            restart_interval = read_dri(fid);
            
        else
            disp(dec2hex(ch));
        end
        
    else
       % disp(dec2hex(ch));
        
    end
    
    ch = fread(fid, 1, 'uint8');
    ftell(fid) % for debugging
    
end