# ***************In the name of GOD *****************

#Developed by soheil gholami & mohsen barzegar

#!/usr/bin/env python


import sys
from math import *
from Tkinter import *


from markers import *
from file_reader import *
import global_vars



def restore_dc(data):
   """ Restore the DC values as the DC values are coded as the difference from the previous DC value of the same component """
   dc_prev= [0 for x in range(len(data[0]))]
   out= []

   # For each MCU
   for mcu in data:
      # For each component
      for comp_num in range(len(mcu)):
         # For each DU
         for du in range(len(mcu[comp_num])):
            if mcu[comp_num][du]:
               mcu[comp_num][du][0]+= dc_prev[comp_num]
               dc_prev[comp_num]= mcu[comp_num][du][0]

      out.append(mcu)

   return out


def dequantify(mcu):
   """ Dequantify MCU """
   #global component

   out= mcu

   # For each component
   for c in range(len(out)):
      # For each DU
      for du in range(len(out[c])):
         # For each coefficient
         for i in range(len(out[c][du])):
            # Multiply by the the corresponding
            # value in the quantization table
            out[c][du][i]*= global_vars.q_table[global_vars.component[c+1]['Tq']][i]

   return out


def zagzig(du):
   """ Put the coefficients in the right order """
   map= [[ 0,  1,  5,  6, 14, 15, 27, 28],
         [ 2,  4,  7, 13, 16, 26, 29, 42],
         [ 3,  8, 12, 17, 25, 30, 41, 43],
         [ 9, 11, 18, 24, 31, 40, 44, 53],
         [10, 19, 23, 32, 39, 45, 52, 54],
         [20, 22, 33, 38, 46, 51, 55, 60],
         [21, 34, 37, 47, 50, 56, 59, 61],
         [35, 36, 48, 49, 57, 58, 62, 63]]

   # Iterate over 8x8
   for x in range(8):
      for y in range(8):
         if map[x][y]<len(du):
            map[x][y]= du[map[x][y]]
         else:
            # If DU is too short
            # This shouldn't happen.
            map[x][y]= 0

   return map


def for_each_du_in_mcu(mcu, func):
   """ Helper function that iterates over all DU's in an MCU and runs "func" on it """
   return [ map(func, comp) for comp in mcu ]


#@memoize
def C(x):
   if x==0:
      return 1.0/sqrt(2.0)
   else:
      return 1.0

# Lookup table to speed up IDCT somewhat
idct_table= [ [(C(u)*cos(((2.0*x+1.0)*u*pi)/16.0)) for x in range(global_vars.idct_precision)] for u in range(global_vars.idct_precision) ]
range8= range(8)
rangeIDCT= range(global_vars.idct_precision)


def idct(matrix):
   """ Converts from frequency domain ordinary(?) """
   out= [ range(8) for i in range(8)]

   # Iterate over every pixel in the block
   for x in range8:
      for y in range8:
         sum= 0

         # Iterate over every coefficient
         # in the DU
         for u in rangeIDCT:
            for v in rangeIDCT:
               sum+= matrix[v][u]*idct_table[u][x]*idct_table[v][y]

         out[y][x]= sum//4

   return out


def expand(mcu, H, V):
   """ Reverse subsampling """
   Hout= max(H)
   Vout= max(V)
   out= [ [ [] for x in range(8*Hout) ] for y in range(8*Vout) ]

   for i in range(len(mcu)):
      Hs= Hout//H[i]
      Vs= Vout//V[i]
      Hin= H[i]
      Vin= V[i]
      comp= mcu[i]

      if len(comp)!=(Hin*Vin):
         return []

      for v in range(Vout):
         for h in range(Hout):
            for y in range(8):
               for x in range(8):
                  out[y+v*8][x+h*8].append(comp[(h//Hs)+Hin*(v//Vs)][y//Vs][x//Hs])

   return out


def combine_mcu(mcu):
   #global num_components

   H= []
   V= []

   for i in range(global_vars.num_components):
      H.append(global_vars.component[i+1]['H'])
      V.append(global_vars.component[i+1]['V'])

   return expand(mcu, H, V)


def combine_blocks(data):
   #global XYP

   X, Y, P= global_vars.XYP

   out= [ [ (0, 0, 0) for x in range(X+32) ] for y in range(Y+64) ]
   offsetx= 0
   offsety= 0

   for block in data:
      ybmax= len(block)
      for yb in range(ybmax):
         xbmax= len(block[yb])
         for xb in range(xbmax):
            out[yb+offsety][xb+offsetx]= tuple(block[yb][xb])
      offsetx+= xbmax
      if offsetx>=X:
         offsetx= 0
         offsety+= ybmax

   return out


def crop_image(data):
   #global XYP
   global Xdensity
   global Ydensity

   X, Y, P= global_vars.XYP
   return [ [ data[y][x] for x in range(X) ] for y in range(Y) ]


def clip(x):
   if x>255:
      return 255
   elif x<0:
      return 0
   else:
      return int(x)

def clamp(x):
    x = (abs(x) + x ) // 2
    if x > 255:
        return 255
    else:
        return x

@memoize
def YCbCr2RGB(Y, Cb, Cr):
   Cred= 0.299
   Cgreen= 0.587
   Cblue= 0.114

   R= Cr*(2-2*Cred)+Y
   B= Cb*(2-2*Cblue)+Y
   G= (Y-Cblue*B-Cred*R)/Cgreen

   return clamp(R+128), clamp(G+128), clamp(B+128)


def YCbCr2Y(Y, Cb, Cr):
   return Y, Y, Y


def for_each_pixel(data, func):
   out= [ [0 for pixel in range(len(data[0]))] for line in range(len(data))]

   for line in range(len(data)):
      for pixel in range(len(data[0])):
         out[line][pixel]= func(*data[line][pixel])

   return out


def tuplify(data):
   out= []

   for line in data:
      out.append(tuple(line))

   return tuple(out)

@memoize
def prepare(x, y, z):
   return "#%02x%02x%02x" % (x, y, z)


def display_image(data):
   #global XYP
 
   X, Y, P= global_vars.XYP

   root= Tk()
   im= PhotoImage(width=X, height=Y)

   im.put(data)

   w= Label(root, image=im, bd=0)
   w.pack()

   mainloop()


input_filename = sys.argv[1]
input_file= open(input_filename, "rb")
in_char= input_file.read(1)

# Find Markers and call suitable function to decode
while in_char:

   if ord(in_char) == 0xff:
      in_char = input_file.read(1)
      in_num = ord(in_char)
      if in_num == 0xd8:
         print("SOI",)

      elif 0xe0 <= in_num <= 0xef:
         print("APP%x" % (in_num-0xe0)),
         decode_app(in_num-0xe0, input_file)

      elif 0xd0<=in_num<=0xd7:
         print("RST%x" % (in_num-0xd0)),

      elif in_num==0xdb:
         print("DQT"),
         decode_dqt(input_file)

      elif in_num==0xdc:
         print("DNL"),
         decode_dnl(input_file)

      elif in_num==0xc4:
         print("DHT"),
         decode_dht(input_file)

      elif in_num==0xc8:
         print("JPG"),

      elif in_num==0xcc:
         print("DAC")

      elif 0xc0<=in_num<=0xcf:
         print("SOF%x" % (in_num-0xc0)),
         decode_sof(in_num-0xc0, input_file)

      elif in_num==0xda:
         print("SOS"),
         decode_sos(input_file)
         global_vars.bit_stream= bit_read(input_file)
         while not global_vars.EOI:
            global_vars.data.append(read_mcu())

      elif in_num==0xd9:
         print("EOI"),

      print("FF%02X" % in_num)

   in_char= input_file.read(1)

input_file.close()

print("AC Huffman tables:", global_vars.h_ac_tables)
print("DC Huffman tables:", global_vars.h_dc_tables)
print("Quantiztion tables:", global_vars.q_table)

if not global_vars.inline_dc:
   print("restore dc")
   global_vars.data= restore_dc(global_vars.data)

print("dequantify")
global_vars.data= map(dequantify, global_vars.data)

print("deserialize")
global_vars.data= [for_each_du_in_mcu(mcu, zagzig) for mcu in global_vars.data]

print("inverse discrete cosine transform")
global_vars.data= [for_each_du_in_mcu(mcu, idct) for mcu in global_vars.data]

print("combine mcu")
global_vars.data= map(combine_mcu, global_vars.data)

print("combine blocks")
global_vars.data= combine_blocks(global_vars.data)

print("crop image")
global_vars.data= crop_image(global_vars.data)

print("color conversion")
global_vars.data= for_each_pixel(global_vars.data, YCbCr2RGB)
#data= for_each_pixel(data, YCbCr2Y)

print("prepare")
global_vars.data= for_each_pixel(global_vars.data, prepare)

print("tuplify")
global_vars.data= tuplify(global_vars.data)

display_image(global_vars.data)
