

import global_vars
from file_reader import *
from memoize import *


def huffman_codes(huffsize):
   """ Calculate the huffman code of each length """
   huffcode= []
   k= 0
   code= 0

   # Magic
   for i in range(len(huffsize)):
      si= huffsize[i]
      for k in range(si):
         huffcode.append((i+1,code))
         code+= 1

      code<<= 1

   return huffcode



def map_codes_to_values(codes, values):
   """ Map the huffman code to the right value """
   out= {}

   for i in range(len(codes)):
      out[codes[i]]= values[i]

   return out


def read_data_unit(comp_num):
    """ Read one DU with component id comp_num """
    #global bit_stream
    #global component
    #global dc

    data = []

    comp = global_vars.component[comp_num]
    huff_tbl = global_vars.h_dc_tables[comp['Td']]

    # Fill data with 64 coefficients
    while len(data) < 64:
        key = 0

        for bits in range(1, 17):
            key_len = []
            key <<= 1
            # Get one bit from bit_stream
            val = get_bits(1, global_vars.bit_stream)
            if val == []:
                break
            key |= val
            # If huffman code exists
            if huff_tbl.has_key((bits, key)):
                key_len = huff_tbl[(bits, key)]
                break

        # After getting the DC value
        # switch to the AC table
        huff_tbl = global_vars.h_ac_tables[comp['Ta']]

        if key_len == []:
            #print((bits, key, bin(key)), "key not found")
            break
        # If ZRL fill with 16 zero coefficients
        elif key_len == 0xF0:
            for i in range(16):
                data.append(0)
            continue

        # If not DC coefficient
        if len(data) != 0:
            # If End of block
            if key_len == 0x00:
                # Fill the rest of the DU with zeros
                while len(data) < 64:
                    data.append(0)
                break

            # The first part of the AC key_len
            # is the number of leading zeros
            for i in range(key_len >> 4):
                if len(data) < 64:
                    data.append(0)
            key_len &= 0x0F

        if len(data) >= 64:
            break

        if key_len != 0:
            # The rest of key_len is the amount
            # of "additional" bits
            val = get_bits(key_len, global_vars.bit_stream)
            if val == []:
                break
            # Decode the additional bits
            num = calc_add_bits(key_len, val)

            # Experimental, doesn't work right
            if len(data) == 0 and global_vars.inline_dc:
                # The DC coefficient value
                # is added to the DC value from
                # the corresponding DU in the
                # previous MCU
                num += global_vars.dc[comp_num]
                global_vars.dc[comp_num] = num

            data.append(num)
        else:
            data.append(0)

    if len(data) != 64:
        print("Wrong size", len(data))

    return data


@memoize
def calc_add_bits(len, val):
   """ Calculate the value from the "additional" bits in the huffman data. """
   if (val & (1 << len-1)):
      pass
   else:
      val-= (1 << len) -1

   return val


def read_mcu():
    """ Read an MCU """
    #global component
    #global num_components
    #global mcus_read

    # comp_num= mcu= range(num_components)
    comp_num = range(global_vars.num_components)
    mcu = [[], [], []]

    # For each component
    for i in comp_num:
        comp = global_vars.component[i + 1]
        mcu[i] = []
        # For each DU
        for j in range(comp['H'] * comp['V']):
            if not global_vars.EOI:
                mcu[i].append(read_data_unit(i + 1))

    global_vars.mcus_read += 1

    return mcu