
'''Functions for reading byte, word(16-bits), non stuffing bits in compressed portion from .jpg file.'''

import global_vars


def read_byte(file):
   """ Read a byte from file """
   out= ord(file.read(1))
   return out

def read_word(file):
   """ Read a 16 bit word from file """
   out= ord(file.read(1)) << 8
   out|= ord(file.read(1))
   return out


def get_bits(num, gen):
   """ Get "num" bits from gen """
   out= 0
   for i in range(num):
      out<<= 1
      val= gen.next()
      if val!= []:
         out+= val & 0x01
      else:
         return []

   return out


def bit_read(file):
    """ Read one bit from file and handle markers and byte stuffing This is a generator function, google it. """
    #global EOI
    #global dc
    #global inline_dc

    input = file.read(1)
    while input and not global_vars.EOI:
        if input == chr(0xFF):
            cmd = file.read(1)
            if cmd:
                # Byte stuffing
                if cmd == chr(0x00):
                    input = chr(0xFF)
                # End of image marker
                elif cmd == chr(0xD9):
                    global_vars.EOI = True
                # Restart markers
                elif 0xD0 <= ord(cmd) <= 0xD7 and global_vars.inline_dc:
                    # Reset dc value
                    global_vars.dc = [0 for i in range(global_vars.num_components + 1)]
                    input = file.read(1)
                else:
                    input = file.read(1)
                    print("CMD: %x" % ord(cmd))

        if not global_vars.EOI:
            for i in range(7, -1, -1):
                # Output next bit
                yield (ord(input) >> i) & 0x01

            input = file.read(1)

    while True:
        yield []