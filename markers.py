
'''Functions to decode various segments in .jpg files'''

from file_reader import *
import global_vars
from huffman import *


def decode_sos(file):
   """ Read the start of scan marker """
   #global component
   #global num_components
   #global dc

   Ls = read_word(file)
   Ls -= 2

   # Read number of components in scan
   Ns = read_byte(file)
   Ls -= 1

   for i in range(Ns):
      # Read the scan component selector
      Cs = read_byte(file)
      Ls -= 1
      # Read the huffman table selectors
      Ta = read_byte(file)
      Ls -= 1
      Td = Ta >> 4
      Ta &= 0xF
      # Assign the DC huffman table
      global_vars.component[Cs]['Td'] = Td
      # Assign the AC huffman table
      global_vars.component[Cs]['Ta'] = Ta

   # Should be zero if baseline DCT
   Ss = read_byte(file)
   Ls -= 1
   # Should be 63 if baseline DCT
   Se = read_byte(file)
   Ls -= 1
   # Should be zero if baseline DCT
   A = read_byte(file)
   Ls -= 1

   print("Ns:%d Ss:%d Se:%d A:%02X" % (Ns, Ss, Se, A))
   global_vars.num_components = Ns
   global_vars.dc = [0 for i in range(global_vars.num_components + 1)]


def decode_sof(type, file):
   """ Read the start of frame marker """
   #global component
   #global XYP

   # Read the marker length
   Lf= read_word(file)
   Lf-= 2
   # Read the sample precision
   P= read_byte(file)
   Lf-= 1
   # Read number of lines
   Y= read_word(file)
   Lf-= 2
   # Read the number of sample per line
   X= read_word(file)
   Lf-= 2
   # Read number of components
   Nf= read_byte(file)
   Lf-= 1

   global_vars.XYP= X, Y, P
   print(global_vars.XYP)

   while Lf>0:
      # Read component identifier
      C= read_byte(file)
      # Read sampling factors
      V= read_byte(file)
      Tq= read_byte(file)
      Lf-= 3
      H= V >> 4
      V&= 0xF
      global_vars.component[C]= {}
      # Assign horizontal sampling factor
      global_vars.component[C]['H']= H
      # Assign vertical sampling factor
      global_vars.component[C]['V']= V
      # Assign quantization table
      global_vars.component[C]['Tq']= Tq



def decode_dht(file):
   """ Read and compute the huffman tables """
   #global h_ac_tables
   #global h_dc_tables

   # Read the marker length
   Lh= read_word(file)
   Lh-= 2
   while Lh>0:
      huffsize= []
      huffval= []
      print("Lh: %d" % Lh)
      T= read_byte(file)
      Th= T & 0x0F
      print("Th: %d" % Th)
      Tc= (T >> 4) & 0x0F
      print("Tc: %d" % Tc)
      Lh= Lh-1

      # Read how many symbols of each length
      # up to 16 bits
      for i in range(16):
         huffsize.append(read_byte(file))
         Lh-= 1

      # Generate the huffman codes
      huffcode= huffman_codes(huffsize)
      print("Huffcode", huffcode)

      # Read the values that should be mapped to
      # huffman codes
      for i in huffcode:
         huffval.append(read_byte(file))
         Lh-= 1

      # Generate lookup tables
      if Tc==0:
         global_vars.h_dc_tables[Th]= map_codes_to_values(huffcode, huffval)
      else:
         global_vars.h_ac_tables[Th]= map_codes_to_values(huffcode, huffval)



def decode_dnl(file):
   """Read the DNL marker Changes the number of lines """
   #global XYP

   Ld= read_word(file)
   Ld-= 2
   NL= read_word(file)
   Ld-= 2

   X, Y, P= global_vars.XYP

   if Y==0:
      global_vars.XYP= X, NL, P


def decode_app(type, file):
   """ Read APP marker """

   # app segment length
   Lp= read_word(file)
   Lp-= 2

   # simply ignore app marker
   file.seek(Lp, 1)




def decode_dqt(file):
   """ Read the quantization table.
       The table is in zigzag order """
   #global q_table

   Lq= read_word(file)
   Lq-= 2
   while Lq>0:
      table= []
      Tq= read_byte(file)
      Pq= Tq >> 4
      Tq&= 0xF
      Lq-= 1

      if Pq==0:
         for i in range(64):
            table.append(read_byte(file))
            Lq-= 1

      else:
         for i in range(64):
            val= read_word(file)
            table.append(val)
            Lq-= 2

      global_vars.q_table[Tq]= table